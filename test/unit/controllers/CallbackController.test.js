var nock = require("nock"),
    request = require('supertest'),
    should = require('should');



describe('CallbackController', function() {
    /*beforeEach(function() {
        nock.recorder.rec();
    });*/
    /*beforeEach(function() {
        nock.disableNetConnect();
    });*/
    describe('index', function() {
        it('should return success', function(done) {
            nock('http://127.0.0.1:37619', {
                    "encodedQueryParams": true
                })
                .get('/callback')
                .reply(200, {
                    "as": "AS7738 Telemar Norte Leste S.A.",
                    "city": "Sao Goncalo",
                    "country": "Brazil",
                    "countryCode": "BR",
                    "isp": "Oi Velox",
                    "lat": -22.8003,
                    "lon": -43.0256,
                    "org": "Oi Velox",
                    "query": "189.106.152.18",
                    "region": "RJ",
                    "regionName": "Rio de Janeiro",
                    "status": "success",
                    "timezone": "America/Sao_Paulo",
                    "zip": ""
                });

            request(sails.hooks.http.app)
                .get('/callback')
                .expect(200, done)
                .end(function(err, res) {
                    should.not.exist(err);
                    done();
                });

        });
        it('should return an error if http service is in error', function(done) {
            nock('http://127.0.0.1:37619', {
                    "encodedQueryParams": true
                })
                .get('/callback')
                .reply(500, {});

            request(sails.hooks.http.app)
                .get('/callback')
                .expect(500, done)
                .end(function(err, res) {
                    should.not.exist(err);
                    done();
                });
        });
        it('should return Error wrongData', function(done) {
            nock('http://127.0.0.1:37619', {
                    "encodedQueryParams": true
                })
                .get('/callback')
                .reply(200, {
                    wrongData: "wrongData",
                });

            request(sails.hooks.http.app)
                .get('/callback')
                .expect(200, done)
                .end(function(err, res) {
                    should.not.exist(err);
                    res.body.should.not.have.property('lat');
                    res.body.should.not.have.property('lon');
                    done();
                });

        });
        it('should return service unavailable', function(done) {
            nock('http://shouldnotexist.com:37619', {
                    "encodedQueryParams": true
                })
                .get('/callback')
                .reply(200, {
                    wrongData: "wrongData",
                });

            request(sails.hooks.http.app)
                .get('/callback')
                .expect(200, done)
                .end(function(err, res) {
                    done(new Error('method should return an error'));
                });
        });
        nock.cleanAll();

    });

});
