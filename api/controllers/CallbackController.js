/**
 * CallbackController
 *
 * @description :: Getting weather forecast on callback way
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	index: function (req, res) {

		var request = require('request');
		var ipURL = 'http://ip-api.com/json';

		request(ipURL, function (error, response, body) {
		    if(error){
		        return res.negotiate(error);
		    }
			if(response.statusCode !== 200){
		        return res.badRequest();
		    }
		    if(body.lat === null || body.lon === null){
		    	return res.badRequest();
		    }

		    var weatherURL = 'http://api.openweathermap.org/data/2.5/weather?lat='+response.lat+'&lon='+response.lon+'&appid=c72b6a3232aa058c8c2055609b24f93f';
		    
		    request(weatherURL, function (error, response, body) {
			    if(error){
			        return res.negotiate(error);
			    }
				if(response.statusCode !== 200){
			    	return res.badRequest();
			    }

			    return res.ok(body);
			});

		});

	}
	
};

