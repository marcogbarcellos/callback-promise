/**
 * PromiseController
 *
 * @description :: Getting weather forecast on callback way
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	index: function (req, res) {
		
		var http = require('http');
		var Q = require("q");
		
		var httpGet = function (opts) {
		     var deferred = Q.defer();
		     http.get(opts, deferred.resolve);
		     return deferred.promise;
		};

		var loadBody = function (response) {
		    var deferred = Q.defer();
		    var body = "";
		    response.on("data", function (chunk) {
		        body += chunk;
		    });
		    response.on("end", function () {
		        deferred.resolve(body);
		    });
		    return deferred.promise;
		};
		
		var ipURL = 'http://ip-api.com/json';
		
		httpGet(ipURL).then(function (response) {
		    var weatherURL = 'http://api.openweathermap.org/data/2.5/weather?lat='+response.lat+'&lon='+response.lon+'&appid=c72b6a3232aa058c8c2055609b24f93f';
		    return httpGet(weatherURL);
		}).then(loadBody).then(function (response) {
		    console.log(response);
		    return res.ok(response);
		}).catch(function (error) {
		    console.error("Couldn't sync to the cloud", error);
		});


	}	
};

